package org.loxageek.luca.rest.boundary;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;

import com.airhacks.rulz.jaxrsclient.JAXRSClientProvider;


public class CursoRestResourceIT{
    static String URL = "http://localhost:8080/curso-rest/resources/personas";
    static final Logger LOGGER = Logger.getLogger(CursoRestResourceIT.class.getSimpleName());

    @Rule
    public JAXRSClientProvider provider = JAXRSClientProvider.buildWithURI(URL);

    @Test
    public void crudTest() {
        String location = create();
        LOGGER.log(Level.INFO, "Location: {0}", location);
        retrieve(location);
        retrieve();
        retrievePersonaNoEncontrada();
    }

    public String create(){
        JsonObjectBuilder builder = Json.createObjectBuilder();
        JsonObject modelEntity = builder.
                add("identificacion", "7777777777").
                add("nombre", "Lucia").
                add("apellido", "Victoria").
                add("genero", "F").
                add("estadoCivil", "S").
                build();
        Response response = provider.
                target().
                request().
                post(Entity.json(modelEntity));
        // 201 CREATED
        assertThat(response.getStatus(), 
                   is(Response.Status.CREATED.getStatusCode()));
        String location = response.getHeaderString("Location");
        return location;
    }

    public void retrieve() {
        Response response = provider.target().
                request(MediaType.APPLICATION_JSON).
                get();
        // 200 OK
        assertThat(response.getStatus(), 
                   is(Response.Status.OK.getStatusCode()));
        JsonArray modelEntities = response.readEntity(JsonArray.class);
        assertFalse(modelEntities.isEmpty());

        JsonObject modelEntity = modelEntities.getJsonObject(0);
        assertTrue(modelEntity.getString("identificacion").equalsIgnoreCase("1111111111"));
        
        modelEntity = modelEntities.getJsonObject(3);
        assertTrue(modelEntity.getString("identificacion").equalsIgnoreCase("4444444444"));
    }

    public void retrieve(String location) {
        JsonObject modelEntity = provider.client().target(location).
                request(MediaType.APPLICATION_JSON).
                get(JsonObject.class);
        String identificacion = modelEntity.getString("identificacion");
        String nombre = modelEntity.getString("nombre");
        
        Assert.assertThat(identificacion, is("7777777777"));
        Assert.assertThat(nombre, is("Lucia"));
    }
    
    public void retrievePersonaNoEncontrada() {
    	String mensaje = provider.
                target().
                path("0000000000").
                request(MediaType.APPLICATION_JSON).
                get(String.class);
                
    	System.out.println(mensaje);
    	
    	Assert.assertTrue(mensaje.startsWith("Persona no en"));
        //Assert.assertThat(mensaje, is("Persona no encontrada"));
    }


}